# the2nerds

## How to host the website

### Step 1: install requirements
`pip install -r requirements.txt`

### Step 2: make a cloudinary account
make a free cloudinary [account here](https://cloudinary.com)

### Step 3: host a postgresql database
#### Recommended: make a free [ElephentSQL account](https://customer.elephantsql.com/login) and host your database online for free
#### or download postgresql [here](https://www.postgresql.org/download/) and host it locally

### Step 4: apply config env vars
`export SECRET_KEY=your_django_secret_key`
<br><br>
`export CLOUDINARY_API_KEY=your_cloudinary_api_key`
<br><br>
`export CLOUDINARY_API_SECRET=your_cloudinary_api_secret`
<br><br>
`export CLOUDINARY_CLOUD_NAME=your_cloudinary_cloud_name`
<br><br>
`export DB_HOST=your_postgresql_db_host`
<br><br>
`export DB_PORT=your_postgresql_db_port`
<br><br>
`export DB_NAME=your_postgresql_db_name`
<br><br>
`export DB_USER=your_postgresql_db_user`
<br><br>
`export DB_PASS=your_postgresql_db_pass`
<br><br>

### Step 5: host it locally
`cd the2nerds`
<br><br>
`python manage.py runserver`

## Buy me a coffee?
[![ko-fi](https://www.ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/memesterhub)
