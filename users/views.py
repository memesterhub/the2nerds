from django.views.generic import CreateView, ListView, UpdateView
from django.contrib.auth.views import LoginView
from . import forms
from . import models
from django.contrib.auth.models import User
from posts import models as post
from django.urls import reverse_lazy
from django.shortcuts import get_object_or_404, render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages


class SignUp(CreateView):
    form_class = forms.CreateUser
    success_url = reverse_lazy('users:users-login')
    template_name = 'users/signup.html'


class Login(LoginView):
    template_name = 'users/login.html'
    redirect_authenticated_user = True


class UserPostList(ListView):
    model = post.Post
    template_name = 'users/user.html'
    context_object_name = 'post'
    ordering = ['-date']
    paginate_by = 4

    def get_queryset(self):
        user = get_object_or_404(User, username=self.kwargs.get('username'))
        return post.Post.objects.filter(author=user).order_by('-date')


def profile(request):

    if request.method == 'POST':
        profile_form = forms.UpdateProfile(
            request.POST, request.FILES, instance=request.user.profile)

        user_form = forms.UpdateUser(request.POST, instance=request.user)

        if profile_form.is_valid() and user_form.is_valid():
            profile_form.save()
            user_form.save()
            messages.success(request, f"Youre account has been updated!")
            return redirect('users:users-profile')

    else:
        profile_form = forms.UpdateProfile(instance=request.user.profile)

        user_form = forms.UpdateUser(instance=request.user)

    context = {
        'profile_form': profile_form,
        'user_form': user_form
    }

    return render(request, 'users/profile.html', context)
