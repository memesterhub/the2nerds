from django.urls import path
from . import views
from django.contrib.auth import views as auth

app_name = 'users'

urlpatterns = [
    path('signup/', views.SignUp.as_view(), name='users-signup'),
    path('login/', views.Login.as_view(), name='users-login'),
    path('logout/', auth.LogoutView.as_view(), name='users-logout'),
    path('user/<str:username>/', views.UserPostList.as_view(), name='users-user'),
    path('profile/', views.profile, name='users-profile')
]
