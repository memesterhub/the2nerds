from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import get_user_model
from . import models

class CreateUser(UserCreationForm):

    class Meta():
        fields = ('username', 'email', 'password1', 'password2')
        model = get_user_model()


class UpdateUser(forms.ModelForm):
    email = forms.EmailField()

    class Meta:
        model = get_user_model()
        fields = ('username', 'email')


class UpdateProfile(forms.ModelForm):
    class Meta:
        model = models.Profile
        fields = ['profile_pic']
