from django.urls import path
from . import views

app_name = 'posts'

urlpatterns = [
    path('posts/new/', views.NewPost.as_view(), name='posts-new'),
    path('posts/<int:pk>/', views.PostDetail.as_view(), name='posts-detail'),
    path('posts/<int:pk>/delete/', views.PostDelete.as_view(), name='posts-del'),
    path('posts/<int:pk>/update/', views.PostUpdate.as_view(), name='posts-update')
]
