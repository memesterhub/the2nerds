from django.db import models
from django.contrib.auth import get_user_model
from django.utils import timezone
from django.urls import reverse_lazy


user = get_user_model()


class Post(models.Model):
    author = models.ForeignKey(user, on_delete=models.CASCADE)
    title = models.CharField(max_length=50)
    content = models.TextField()
    date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return f"{self.title} Posted by {self.author}"

    def get_absolute_url(self):
        return reverse_lazy('posts:posts-detail', kwargs={'pk': self.pk})
