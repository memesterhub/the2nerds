from django.views.generic import CreateView, DetailView, DeleteView, UpdateView
from . import models
from django.urls import reverse_lazy
from django.contrib.auth.mixins import UserPassesTestMixin, LoginRequiredMixin


class NewPost(LoginRequiredMixin, CreateView):
    model = models.Post
    fields = ('title', 'content')
    template_name = 'posts/new.html'
    success_url = reverse_lazy('social:social-home')

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class PostDetail(DetailView):
    model = models.Post
    template_name = 'posts/post.html'
    context_object_name = 'post'


class PostDelete(UserPassesTestMixin, DeleteView):
    model = models.Post
    template_name = 'posts/del.html'
    success_url = reverse_lazy('social:social-home')

    def test_func(self):

        post = self.get_object()

        return self.request.user == post.author


class PostUpdate(UserPassesTestMixin, UpdateView):
    model = models.Post
    fields = ('title', 'content')
    template_name = 'posts/update.html'

    def test_func(self):

        post = self.get_object()

        return self.request.user == post.author
