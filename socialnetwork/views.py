from django.views.generic import TemplateView, ListView
from posts import models


class Home(ListView):
    model = models.Post
    template_name = 'social/index.html'
    context_object_name = 'post'
    ordering = ['-date']
    paginate_by = 4


class About(TemplateView):
    template_name = 'social/about.html'
