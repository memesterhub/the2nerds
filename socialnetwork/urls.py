from django.urls import path, include
from . import views

app_name = 'social'

urlpatterns = [
    path('', views.Home.as_view(), name='social-home'),
    path('about/', views.About.as_view(), name='social-about'),
]
